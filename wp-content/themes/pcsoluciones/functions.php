<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/utils.php',                 // Utility functions
  'lib/init.php',                  // Initial theme setup and constants
  'lib/wrapper.php',               // Theme wrapper class
  'lib/conditional-tag-check.php', // ConditionalTagCheck class
  'lib/config.php',                // Configuration
  'lib/assets.php',                // Scripts and stylesheets
  'lib/titles.php',                // Page titles
  'lib/extras.php',                // Custom functions
  ];

  foreach ($sage_includes as $file) {
    if (!$filepath = locate_template($file)) {
      trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
    }

    require_once $filepath;
  }
  unset($file, $filepath);

  function comment_foro_excel($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
     <div id="comment-<?php comment_ID(); ?>">
       <div>
        <?php $user_name_str = substr(get_comment_author(),0, 20); ?>
        <div class="enviado"><?php printf(__('<cite><b> Por: %s </b></cite> -'), $user_name_str); ?> <?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()); ?></div>
      </div>
      <?php if ($comment->comment_approved == '0') : ?>
       <em><?php _e('Este comentario no ha sido aprobado'); ?></em>
       <br />
     <?php endif; ?>
     <?php comment_text(); ?>
   </div>
   <?php
 }


 /* agrega una funcion luego del envio del formulario */
 add_action( 'wpcf7_before_send_mail', 'process_contact_form_data' );
 /* Funcion que valida el formulario y llama la funcion para crear el nodo */
 function process_contact_form_data( $contact_data ){
  if ($contact_data->id == 49) {
    create_custom_post_type($contact_data);
  }

}
/* funcion que crea el nodo con los datos del formulario */
function create_custom_post_type($contact_data){

  $submission = WPCF7_Submission::get_instance();
  if ( $submission ) {
    $posted_data = $submission->get_posted_data();
  }

  $usuario = $posted_data["usuario"];
  $pregunta = $posted_data["pregunta"];

  $foro_excel_new_post = array(
    'post_title'   =>  $pregunta,
    'post_content' =>  'Aún no se ha respondido la pregunta',
    'post_status'  =>  'pending',
    'post_type'    => 'post',
    );

  /* crea el nodo */
  $new_foro_post_id = wp_insert_post($foro_excel_new_post);
  
  /* inserta el valor necesario en el campo custom según el id del nodo */
  //__update_post_meta($new_foro_post_id, 'usuario_foro', $usuario);
  update_post_meta( $new_foro_post_id, '_simple_fields_fieldGroupID_1_fieldID_1_numInSet_0', $usuario);
  //add_post_meta (get_the_id (), '_simple_fields_fieldGroupID_32_fieldID_3_numInSet_0'," test ");
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active ';
     }
     return $classes;
}