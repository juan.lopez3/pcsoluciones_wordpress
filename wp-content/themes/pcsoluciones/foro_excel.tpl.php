<?php 
	/*
		Template Name: Foro Excel
	*/
		?>

		<div class="content-foro">
			<!-- Se listan todos los contenidos de tipo Foro de excel -->
			<div class="main-title" id="foro"><h3 >FORO DE EXCEL <span class="bajando glyphicon glyphicon-triangle-bottom" aria-label="Left Align"></span></h3></div>
			<table>
				<thead>
					<tr>
						<th>TEMA</th>
						<th>Comentarios</th>
					</tr>
				</thead>

				<?php 
				/* Se consultan los post de tipo foro_de_excel y se reccorren */
				query_posts('post_type=post');
				while (have_posts()) : the_post(); 
				?>
				<tbody>
					<td>
						<h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<div class="autor"><p class="byline author vcard"><?= __('Por:', 'sage'); ?> <span href= rel="author" class="fn"><?php print(simple_fields_value("usuario",$postID)); ?></span></p></div>
					</td>

					<td><?php comments_number(); ?></td>	
				</tbody>
			<?php endwhile; ?>
		</table>

	</div>











