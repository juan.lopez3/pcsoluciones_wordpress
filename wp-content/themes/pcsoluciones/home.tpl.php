<?php 
	/*
		Template Name: Home
	*/
?>


<!-- Se agerga el slider -->
<?php putRevSlider( "sliderhome" ) ?>
<!-- Se agerga el slider -->

<!-- Se listan todos los contenidos de tipo servicio -->
<?php include 'templates/home-servicios.tpl.php'; ?>
<!-- Se listan todos los contenidos de tipo servicio -->


<!-- Se listan todos los contenidos de tipo fortaleza -->
<?php include 'templates/home-fortalezas.tpl.php'; ?>
<!-- Se listan todos los contenidos de tipo fortaleza -->


<!-- Se listan todos los contenidos de tipo Cliente -->
<?php include 'templates/home-clientes.tpl.php'; ?>
<!-- Se listan todos los contenidos de tipo Cliente -->


<!-- Se listan todos los contenidos de tipo Foro de excel -->
<?php include 'templates/home-post.tpl.php'; ?>
<!-- Se listan todos los contenidos de tipo Foro de excel -->

