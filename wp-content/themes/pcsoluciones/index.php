
<!-- get the header file -->
<?php get_template_part('templates/page', 'header'); ?>


<div class="block-nav-line">
	<div class="login-user">
		<!-- links for user login -->
		<?php if(!is_user_logged_in() ) : ?>
			<a href="<?php print(get_site_url() . '/wp-admin'); ?>">login</a>
		<?php else : ?>
			<a href="<?php print(wp_logout_url()); ?>">Logout</a>
		<?php endif; ?>
		<!-- end links for user login -->

	</div>
	<div class="search-form">
		<!-- search form block -->
		<?php get_search_form(); ?>
		<!-- search form block -->
	</div>
</div>

<!-- call all post -->

<?php if (!have_posts()) : ?>
	<div class="alert alert-warning">
		<?php _e('Sorry, no results were found.', 'sage'); ?>
	</div>
<?php endif; ?>


	<!-- recorre todos los post que encuentre -->
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
	<?php endwhile; ?>
	<!-- recorre todos los post que encuentre -->

<!-- call all post -->

<?php the_posts_navigation(); ?>