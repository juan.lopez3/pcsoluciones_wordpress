<?php while (have_posts()) : the_post(); ?>

  <div class="conozcanos-title">
  	<?php get_template_part('templates/page', 'header'); ?>
  </div>
  <div class="conozcanos-section">
  	<?php get_template_part('templates/content', 'page'); ?>
  </div>
<?php endwhile; ?>
