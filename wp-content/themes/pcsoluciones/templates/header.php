<!--  Tpl header   -->

<div id="big-container">
    <header>
      <div class="infiContac">
        <p class="direccion">Cra 15# 74-45 Of. 401 Bogotá- Colombia</p>
        <p class="telefonos">346 35 38 | Cel. (310) 311 86 83</p>
      </div>
  <nav >
        <div class="container" >
          
          <!-- Grid  navbar -->
          <div class="navbar navbar-default yamm ">
            <div class="navbar-header">
              <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        <div class="logo-h">
            <a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo("template_url"); ?>/assets/img/logoPC.png" alt="" class="img-responsive"></a>
        </div>
            </div>
            <div id="navbar-collapse-grid" class="navbar-collapse collapse">


            <div class="access">
              <?php 
 
              wp_nav_menu([
                'menu' => 'main-menu', 
                'menu_class' => 'nav navbar-nav navbar-right'
                ]);
              ?>
            </div>



            </div>
          </div>
        </div>
   </nav>
  
</header>
</div>