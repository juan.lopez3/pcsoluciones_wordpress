          <div class="bloque-formulario">

            <?php $comment_args = array( 'title_reply'=>'Su comentario:',

              'fields' => apply_filters( 'comment_form_default_fields', array(

                'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Nombre*' ) . '</label> ' . ( $req ? '<span>*</span>' : '' ) .

                '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',   

                'email'  => '<p class="comment-form-email">' .

                '<label for="email">' . __( 'Correo electronico*' ) . '</label> ' .

                ( $req ? '<span>*</span>' : '' ) .

                '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />'.'</p>',

                'url'    => '' ) ),

              'comment_field' => '<p>' .

              '<label for="comment">' . __( 'Respuesta' ) . '</label>' .

              '<textarea id="comment" ' . $aria_req .' name="comment" cols="45" rows="8" aria-required="true"></textarea>' .

              '</p>',

              'comment_notes_after' => '',

              );

              comment_form($comment_args); ?>
            </div>