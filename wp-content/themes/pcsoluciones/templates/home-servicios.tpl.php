
<!-- Se listan todos los contenidos de tipo servicio -->
<div class="int-container services-container" id="servicios">
	<div class="main-title" ><h3>SERVICIOS <span class="bajando glyphicon glyphicon-triangle-bottom" aria-label="Left Align"></span></h3> </div>
	<div class="ocultarServicios">
		<?php 
		/* Se consultan los post de tipo servicio y se reccorren */
		query_posts('post_type=servicio');
		while (have_posts()) : the_post(); 
		?>
		<div class="service item-servicio">
			<div id="testText" class="servicios-img">
				<!-- Se imprime el campo icono -->
				<?php print(get('icono')); ?>
				<!-- Llamar la letra poe la taxonomia -->
			</div>
			<!-- Se imprime el título -->
			<h2 class="title-services"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<!-- Se imprime el campo body -->
			<?php the_content(); ?>
			<a href="<?php the_permalink(); ?>"><div id="testText" class="mas">k</div></a>
		</div><!-- service-1 -->
		<?php endwhile; ?>
	</div>
</div><!-- container -->
