
<!-- Se listan todos los contenidos de tipo post -->

<div class="int-container forum-container" id="foro">
	<div class="main-title" ><h3 >FORO DE EXCEL <span class="bajando glyphicon glyphicon-triangle-bottom" aria-label="Left Align"></span></h3></div>

	<div class="foroOculto">
		<div class="foro-margen">
			<div class="row-forum">
				<div class="container-forum">	
					<?php 
					/* Se consultan los post de tipo foro_de_excel y se reccorren */
					query_posts('post_type=post');
					while (have_posts()) : the_post(); 
					?>
					<div class="item-foro">
						<!-- Se imprime el título -->
						<h3 class="title-forum"><span id="testText" class="iten-foro">f</span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<!-- Se imprime el campo body -->
						<div class="forum-body">

						<?php //$trimmed = wp_trim_words( the_content(), 55, $more = 'null' ); ?>

						<?php the_excerpt(); ?>

						</div>
					</div><!-- service-1 -->
				<?php endwhile; ?>
			</div>
		</div><!-- row-forum -->

		<div class="main-title mar-ver-todo"  id="foro"><h3><a href="<?php print(get_page_link(53)); ?>" target="_blank">Ver todo el foro</a></h3></div>

		<hr id="line-foro-ex">
		<div class="content-form-forum"><!-- content-form-forum -->
			<p class="description-forum">En este espacio esperamos sus preguntas sobre el manejo de datos relacionados con salud y seguridad en el trabajo utilizando Excel.</p>
			<?php print( do_shortcode( '[contact-form-7 id="49" title="Formulario foro excel"]' )); ?>
			<p class="mandatory">Su información personal no será publicada en la página.</p>
		</div><!-- content-form-forum -->
	</div>
</div> <!--oculto -->
</div><!--forum container -->
