
<!-- Se listan todos los contenidos de tipo Cliente -->
<div class="int-container customers-container" id="clientes">
	<div class="main-title" ><h3>PRINCIPALES CLIENTES <span class="bajando glyphicon glyphicon-triangle-bottom" aria-label="Left Align"></span></h3></div>
	<div class="ocultarClientes">

		<div class="client-row">
			<?php 
			query_posts('post_type=cliente');
			while (have_posts()) : the_post(); 
			?>
			<div class="client client-1"><img src="<?php print(get('imagen')); ?>" alt="logo-"></div>
		<?php endwhile; ?>
	</div>

</div>
</div><!-- container -->
