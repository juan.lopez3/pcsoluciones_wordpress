<!-- Se listan todos los contenidos de tipo fortaleza -->
<div class="int-container strengths-container" id="fortalezas">
	<div class="main-title" ><h3>FORTALEZAS <span class="bajando glyphicon glyphicon-triangle-bottom" aria-label="Left Align"></span></h3></div>
	<div class="ocultarFortalezas">
		<?php 
		query_posts('post_type=fortaleza');
		while (have_posts()) : the_post(); 
		?>
		<div class="stren stren-a">
			<?php the_content(); ?>
		</div>
	<?php endwhile; ?>
</div>
</div><!-- container -->
