<?php
if (post_password_required()) {
  return;
}
?>

<section id="comments" class="comments">
  <div class="respuesta">
     <h1>Comentarios</h1>
     <br/>
    <?php if (have_comments()) : ?>
     <div class="comment-list">
      <?php wp_list_comments(['short_ping' => true , 'callback' => 'comment_foro_excel']); ?>
    </div>

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
      <nav>
        <ul class="pager">
          <?php if (get_previous_comments_link()) : ?>
            <li class="previous"><?php previous_comments_link(__('&larr; Older comments', 'sage')); ?></li>
          <?php endif; ?>
          <?php if (get_next_comments_link()) : ?>
            <li class="next"><?php next_comments_link(__('Newer comments &rarr;', 'sage')); ?></li>
          <?php endif; ?>
        </ul>
      </nav>
    <?php endif; ?>
  <?php endif; // have_comments() ?>
  <?php if (!comments_open() && get_comments_number() != '0' && post_type_supports(get_post_type(), 'comments')) : ?>
    <div class="alert alert-warning">
      <?php _e('Comments are closed.', 'sage'); ?>
    </div>
  <?php endif; ?>
</div>
</section>
