<!-- 
    Template interna foro de excel
  -->




  <?php while (have_posts()) : the_post(); ?>
    <article <?php post_class(); ?>>
      <!-- Hay que llamar el nombre digitado en el formulario -->
      <div class="foroWrapper">
        <div class="foroHeader">
          <?php get_template_part('templates/entry-meta-foro-excel'); ?>
          <?php 
          //$data = simple_cck::get_post_data($post->ID); 
          ?>
          <p><?php print(simple_fields_value("usuario",$postID)); ?>, pregunta:</p>

        </div>
        <div class="foroBody">
          <div class="preguntaPrincipal">
            <div class=""><h2 class="labelPregunta">Pregunta</h2><?php the_title(); ?></div>
          </div>
          <div class="respuestas respuestaAdmin">
            <?php the_content(); ?>
          </div>
          <div class="respuestas respuesta">
            <?php comments_template('/templates/comments-foro-excel.php'); ?>
          </div>
        </div>
        <div class="formWrapper">
          
          <?php include 'templates/comentarios-secundarios-foro-excel.php'; ?>

          </div>
        </div>
      </article>
    <?php endwhile; ?>