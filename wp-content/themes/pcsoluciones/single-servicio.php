

<?php while (have_posts()) : the_post(); ?>

  <?php 
  /* clase csutom para post que no tienen imagen */
  $imagen = get('imagen_servicio');
  $image_class = '';
  if ($imagen == '') {
    $image_class = 'no-imagen';
  }else{
     $image_class = 'si-imagen';
  }
  ?>

  <article <?php post_class(); ?> class="interna">
    <div class="<?php print($image_class); ?>">


      <div id="testText" class="servicios-img">
        <!-- Se imprime el campo icono -->
        <?php print(get('icono')); ?>
        <!-- Llamar la letra poe la taxonomia -->
      </div>
      <div class="main-title">
        <h3 class="entry-title"><?php the_title(); ?></h3>
      </div>
      <div class="entry-content">
        <?php the_content(); ?>
      </div>
      <div class="img-servicio-interna">
        <img src="<?php print(get('imagen_servicio')); ?>" alt="">
      </div>
    </div>
  </article>
  <?php $image_class = ''; ?>
<?php endwhile; ?>

